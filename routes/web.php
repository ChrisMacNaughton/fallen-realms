<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('register', function() {
//     return view('register');
// });

Route::get('terms', function() {
    return view('terms');
})->name('terms');

Route::get('connecting', function() {
    return view('connecting');
})->name('connecting');

// });
// Route::post('register', 'Auth\RegisterController@create');

// Route::get('/login', function() {
//     return view('login');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
