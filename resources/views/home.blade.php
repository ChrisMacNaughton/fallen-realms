@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome</div>
                <div class="card-body">
                    <p>{{ config('app.name') }} is a new private server that launched 25 June, 2018 with Wrath of the Lich King supprt.</p>
                    <p>Our initial server, Savage Plains, is a PvE server with 7x experience rates, and increased drop rates to support that.</p>
                </div>
            </div>
                @guest

                @else

                @endguest
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Online Players</div>
                <div class="card-body">
                    <dl class="dl dl-inline">
                    @foreach ($realms as $realm)
                       <dt>{{ $realm['name'] }}</dt>
                       <dd>{{ $realm['Characters']}} {{ str_plural('player', $realm['Characters'] ) }}</dd>
                    @endforeach
                    </dl>
                </div>
            </div>
                @guest

                @else

                @endguest
        </div>
    </div>
</div>
@endsection
