@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                        <div class="card">
                <div class="card-header">How To Connect</div>
                <div class="card-body">
                    <ol>
                        <!-- <li>First of all, you must create an account. The account is used to log into both the game and our website.</li> -->

                        <li>Install World of Warcraft (Patch 3.3.5a). (<a href="https://thepiratebay.pet/torrent/6687518/WOW_Wotlk_3.3.5a">Torrent Link</a>)</li>

                        <li>If you already have the client, Open up the "World of Warcraft" directory. The default directory is "C:\Program Files\World of Warcraft".</li>

                        <li>Open up the file called "realmlist.wtf" with a text editor such as Notepad. "realmlist.wtf" will be located in the directory `Data/enGB` underneath your primary "World of Warcraft" directory. To do this, you must right click on the file and choose properties, then select notepad as the default software for files with the ".wtf" ending. You may also just start the text editor and drag the file into the edit window.</li>

                        <li><p>Erase all text and change it to:</p>

                        <pre><code>
set realmlist logon.thefallenrealms.com
set realmname "Fallen Realms"
                        </code></pre>

                        <p>Save the realmlist.wtf file and you may now start playing!</p>

                        </p><strong>Important</strong>: Please delete your CACHE files for a proper gameplay.</p>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
