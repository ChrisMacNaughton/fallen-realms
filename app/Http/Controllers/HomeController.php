<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $realms = [
            [
                "name" => "Savage Plains",
                // "GMs" => DB::connection('characters')->select(
                //     "SELECT COUNT(guid) AS count
                //     FROM characters.characters
                //     JOIN auth.account ON account.id = characters.account
                //     JOIN auth.account_access ON account_access.id = account.id
                //     WHERE characters.online=1")[0]->count,
                "Characters" => DB::connection('characters')->table('characters')->where('online', 1)->count(),
                // DB::connection('characters')->table('realmlist')->select('name', 'population')->get(),
            ]
        ];
        return view('home')->with('realms', $realms);
    }
}
