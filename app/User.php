<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'sha_pass_hash',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'sha_pass_hash', 'remember_token',
    ];

    protected $connection = 'auth';
    protected $table = 'account';
    public $timestamps = false;

    public function getAuthPassword()
    {
        return $this->sha_pass_hash;
    }

    /**
    * Overrides the method to ignore the remember token.
    */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
          parent::setAttribute($key, $value);
        }
    }

    public function gmlevel()
    {
        if( is_null($this->level) ) {
            $query_level = DB::connection('auth')->table('account_access')->where('id', $this->id)->select('gmLevel')->first();
            if ($query_level) {
                $this->level = $query_level->gmLevel;
            } else {
                $this->level = false;
            }
        }
        return $this->level;
    }
}
