<?php

namespace App;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class ShaHasher implements HasherContract {

    public function info($value) {
        return [];
    }

    public function make($value, array $options = array()) {
        // $value = env('SALT', '').$value;
        return strtoupper(sha1($value));
    }

    public function check($value, $hashedValue, array $options = array()) {
        print_r("Doing stuff");
        return $this->make($value) === $hashedValue;
    }

    public function needsRehash($hashedValue, array $options = array()) {
        return false;
    }

}
